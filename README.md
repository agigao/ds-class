# Data-Science Class
This Fall I'm taking a data-science class at college, and this repo will capture some materials along the way as class progresses.

## Some info 

Programming language: R

Sources: 
- https://www.datacamp.com/
- https://www.openintro.org/stat/labs.php?stat_lab_software=R

Class: Introduction to Data-Science w/R and Tableau

Professor: Michael D. Harris
